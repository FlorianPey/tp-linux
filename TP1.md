# Résultat du TP1 de Peyron Florian

## Sommaire

[TOC]



### Carte réseau 1

Configurations de la carte réseau principal.

```bash
[flo@tp1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-ens192 
TYPE=Ethernet
BOOTPROTO=static
IPV6INIT=yes
NAME=ens192
DEVICE=ens192
ONBOOT=yes
IPADDR=10.0.1.170
NETMASK=255.255.255.0
NETWORK=10.0.1.0
BROADCAST=10.0.1.255
GATEWAY=10.0.1.1
```



### Carte réseau 2

Configurations de la carte réseau secondaire.

```bash
[flo@tp1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-ens224 
TYPE=Ethernet
BOOTPROTO=static
DEFROUTE=yes
IPV6INIT=yes
NAME=ens224
DEVICE=ens224
ONBOOT=yes
IPADDR=192.168.15.5
NETMASK=255.255.255.0
```



### IP a

Résultat de la conf IP :

```bash
[flo@tp1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens192: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:50:56:bb:6b:72 brd ff:ff:ff:ff:ff:ff
    inet 10.0.1.170/24 brd 10.0.1.255 scope global noprefixroute ens192
       valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:febb:6b72/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: ens224: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:50:56:bb:db:da brd ff:ff:ff:ff:ff:ff
    inet 192.168.15.5/24 brd 192.168.15.255 scope global noprefixroute ens224
       valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:febb:dbda/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```



### Accès web

Accès à internet :

```bash
[flo@tp1 ~]$ ping google.fr
PING google.fr (216.58.214.3) 56(84) bytes of data.
64 bytes from ams17s09-in-f3.1e100.net (216.58.214.3): icmp_seq=1 ttl=112 time=78.7 ms
64 bytes from ams17s09-in-f3.1e100.net (216.58.214.3): icmp_seq=2 ttl=112 time=87.1 ms
64 bytes from ams17s09-in-f3.1e100.net (216.58.214.3): icmp_seq=3 ttl=112 time=73.6 ms
64 bytes from ams17s09-in-f3.1e100.net (216.58.214.3): icmp_seq=4 ttl=112 time=73.6 ms
64 bytes from ams17s09-in-f3.1e100.net (216.58.214.3): icmp_seq=5 ttl=112 time=82.4 ms
^C
--- google.fr ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 73.620/79.113/87.131/5.219 ms
```



### SSH

La clef publique de mon ordinateur sur la VM.

```bash
[flo@tp1 ~]$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2E***ADAQABAAABAQDWj***Jj7toIeJzFp9FoxxeuCARNQwO5uB**********UAPxdLZsfwmCdIRbisyY*****r5MSBIA0tGprwq7RaUlP9OfN1bh*****Qks5CgiLGzP8cF1TjR4bG+XY+0BQ5jU6XARsEdRvRqisgm6ot/Oc**********H0Qtqt4flvxWm7NaOZcF**7d8GouCvakFH12l95R4GvvzgZQ3**********f1n3p0wt2Fa1Gjbh5Cf1Sd72iywsPZYrYtZntE+**********o/HrFeQr7F8JrTIa9VBJ*****KTJOukRDgixxfODe8NmmD8iilAaU*****C0mP4j/keg9tR florianpeyron@MacBook-Pro-de-Florian.local
```

J'ai glissé quelques ***, même si c'est ma clès publique :)



### Passwd

Liste des users :

```bash
[flo@tp1 ~]$ sudo cat /etc/passwd
[sudo] Mot de passe de flo : 
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:999:998:User for polkitd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
flo:x:1000:1000:flo:/home/flo:/bin/bash
tata:x:1001:1001::/home/tata:/bin/bash
toto:x:1002:1002::/home/toto:/bin/sh
web:x:1003:1004::/home/web:/bin/bash
```



### Group

Liste des groupes et leurs membres :

````bash
[flo@tp1 ~]$ sudo cat /etc/group
root:x:0:
bin:x:1:
daemon:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mem:x:8:
kmem:x:9:
wheel:x:10:flo
cdrom:x:11:
mail:x:12:postfix
man:x:15:
dialout:x:18:
floppy:x:19:
games:x:20:
tape:x:33:
video:x:39:
ftp:x:50:
lock:x:54:
audio:x:63:
nobody:x:99:
users:x:100:
utmp:x:22:
utempter:x:35:
input:x:999:
systemd-journal:x:190:
systemd-network:x:192:
dbus:x:81:
polkitd:x:998:
ssh_keys:x:997:
sshd:x:74:
postdrop:x:90:
postfix:x:89:
flo:x:1000:flo
tata:x:1001:
toto:x:1002:
admins:x:1003:toto,tata,flo
web:x:1004:
````



### Sudoers

Permission du groupes admin :

```bash
[flo@tp1 ~]$ sudo cat /etc/sudoers
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
## 
## This file must be edited with the 'visudo' command.

## Host Aliases
## Groups of machines. You may prefer to use hostnames (perhaps using 
## wildcards for entire domains) or IP addresses instead.
# Host_Alias     FILESERVERS = fs1, fs2
# Host_Alias     MAILSERVERS = smtp, smtp2

## User Aliases
## These aren't often necessary, as you can use regular groups
## (ie, from files, LDAP, NIS, etc) in this file - just use %groupname 
## rather than USERALIAS
# User_Alias ADMINS = jsmith, mikem


## Command Aliases
## These are groups of related commands...

## Networking
# Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /bin/ping, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool

## Installation and management of software
# Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum

## Services
# Cmnd_Alias SERVICES = /sbin/service, /sbin/chkconfig, /usr/bin/systemctl start, /usr/bin/systemctl stop, /usr/bin/systemctl reload, /usr/bin/systemctl restart, /usr/bin/systemctl status, /usr/bin/systemctl enable, /usr/bin/systemctl disable

## Updating the locate database
# Cmnd_Alias LOCATE = /usr/bin/updatedb

## Storage
# Cmnd_Alias STORAGE = /sbin/fdisk, /sbin/sfdisk, /sbin/parted, /sbin/partprobe, /bin/mount, /bin/umount

## Delegating permissions
# Cmnd_Alias DELEGATING = /usr/sbin/visudo, /bin/chown, /bin/chmod, /bin/chgrp 

## Processes
# Cmnd_Alias PROCESSES = /bin/nice, /bin/kill, /usr/bin/kill, /usr/bin/killall

## Drivers
# Cmnd_Alias DRIVERS = /sbin/modprobe

# Defaults specification

#
# Refuse to run if unable to disable echo on the tty.
#
Defaults   !visiblepw

#
# Preserving HOME has security implications since many programs
# use it when searching for configuration files. Note that HOME
# is already set when the the env_reset option is enabled, so
# this option is only effective for configurations where either
# env_reset is disabled or HOME is present in the env_keep list.
#
Defaults    always_set_home
Defaults    match_group_by_gid

# Prior to version 1.8.15, groups listed in sudoers that were not
# found in the system group database were passed to the group
# plugin, if any. Starting with 1.8.15, only groups of the form
# %:group are resolved via the group plugin by default.
# We enable always_query_group_plugin to restore old behavior.
# Disable this option for new behavior.
Defaults    always_query_group_plugin

Defaults    env_reset
Defaults    env_keep =  "COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS"
Defaults    env_keep += "MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE"
Defaults    env_keep += "LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES"
Defaults    env_keep += "LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE"
Defaults    env_keep += "LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY"

#
# Adding HOME to env_keep may enable a user to run unrestricted
# commands via sudo.
#
# Defaults   env_keep += "HOME"

Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin

## Next comes the main part: which users can run what software on 
## which machines (the sudoers file can be shared between multiple
## systems).
## Syntax:
##
## 	user	MACHINE=COMMANDS
##
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere 
root	ALL=(ALL) 	ALL

## Allows members of the 'sys' group to run networking, software, 
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel	ALL=(ALL)	ALL
%admins  ALL=(ALL)       ALL

## Same thing without a password
# %wheel	ALL=(ALL)	NOPASSWD: ALL

## Allows members of the users group to mount and unmount the 
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now

## Read drop-in files from /etc/sudoers.d (the # here does not mean a comment)
#includedir /etc/sudoers.d
```



### Nom de domaine

Configuration du nom FQDN de la machine.

```bash
[flo@tp1 ~]$ cat /etc/hostname 
tp1.florian.net
```



### Resolv.conf

Configuration DNS

```bash
[flo@tp1 ~]$ sudo cat /etc/resolv.conf 
nameserver 10.0.1.110
```

C'est un de mes proxy/firewall dns perso (PiHole)



### Lvdisplay

Liste des volumes logiques LVM :

```bash
[flo@tp1 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/lab/volume1
  LV Name                volume1
  VG Name                lab
  LV UUID                nnCRUr-3WlM-B39A-GH3Z-vjbK-NMrb-lPW5Lg
  LV Write Access        read/write
  LV Creation host, time tp1.florian.net, 2020-12-15 15:48:46 +0100
  LV Status              available
  # open                 1
  LV Size                1,00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
   
  --- Logical volume ---
  LV Path                /dev/lab/volume2
  LV Name                volume2
  VG Name                lab
  LV UUID                M15FcG-xdBj-K4Cw-c8d8-EJ6b-U332-CzLDkd
  LV Write Access        read/write
  LV Creation host, time tp1.florian.net, 2020-12-15 15:48:54 +0100
  LV Status              available
  # open                 1
  LV Size                1,00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3
   
  --- Logical volume ---
  LV Path                /dev/lab/volume3
  LV Name                volume3
  VG Name                lab
  LV UUID                b03YpO-aegA-uWm4-XtRO-Fxw2-mx0E-uBFnhl
  LV Write Access        read/write
  LV Creation host, time tp1.florian.net, 2020-12-15 15:49:00 +0100
  LV Status              available
  # open                 1
  LV Size                1,00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:4
   
  --- Logical volume ---
  LV Path                /dev/centos/swap
  LV Name                swap
  VG Name                centos
  LV UUID                yYht1d-cwVU-c2NO-6rKp-TwkG-ngA1-y5t9P3
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2020-12-14 14:32:56 +0100
  LV Status              available
  # open                 2
  LV Size                1,60 GiB
  Current LE             410
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1
   
  --- Logical volume ---
  LV Path                /dev/centos/root
  LV Name                root
  VG Name                centos
  LV UUID                0zAgdd-5jPa-9p6r-YMLm-MZAW-wulx-5pbWpk
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2020-12-14 14:32:56 +0100
  LV Status              available
  # open                 1
  LV Size                13,39 GiB
  Current LE             3429
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
```



### Mount

Liste des montages effectifs

```bash
[flo@tp1 ~]$ mount
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime,seclabel)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
devtmpfs on /dev type devtmpfs (rw,nosuid,seclabel,size=495596k,nr_inodes=123899,mode=755)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev,seclabel)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,seclabel,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,nodev,seclabel,mode=755)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,seclabel,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,net_prio,net_cls)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,freezer)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,devices)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,memory)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,hugetlb)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpuacct,cpu)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpuset)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,pids)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,blkio)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,perf_event)
configfs on /sys/kernel/config type configfs (rw,relatime)
/dev/mapper/centos-root on / type xfs (rw,relatime,seclabel,attr2,inode64,noquota)
selinuxfs on /sys/fs/selinux type selinuxfs (rw,relatime)
systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=33,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=12063)
fusectl on /sys/fs/fuse/connections type fusectl (rw,relatime)
mqueue on /dev/mqueue type mqueue (rw,relatime,seclabel)
debugfs on /sys/kernel/debug type debugfs (rw,relatime)
hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime,seclabel)
/dev/sda1 on /boot type xfs (rw,relatime,seclabel,attr2,inode64,noquota)
/dev/mapper/lab-volume1 on /mnt/part1 type ext4 (rw,relatime,seclabel,data=ordered)
/dev/mapper/lab-volume2 on /mnt/part2 type ext4 (rw,relatime,seclabel,data=ordered)
/dev/mapper/lab-volume3 on /mnt/part3 type ext4 (rw,relatime,seclabel,data=ordered)
tmpfs on /run/user/1000 type tmpfs (rw,nosuid,nodev,relatime,seclabel,size=101488k,mode=700,uid=1000,gid=1000)
```



### Fstab

Liste des montages crées au démarrage

```bash
[flo@tp1 ~]$ sudo cat /etc/fstab 

#
# /etc/fstab
# Created by anaconda on Mon Dec 14 14:32:57 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=e091c57c-3988-42db-86c7-e9c8868b2ae3 /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/mapper/lab-volume1 /mnt/part1							ext4		defaults				0 0
/dev/mapper/lab-volume2 /mnt/part2              ext4    defaults        0 0
/dev/mapper/lab-volume3 /mnt/part3              ext4    defaults        0 0
```



### web.service

Le contenu du fichier web.service :

```bash
[flo@tp1 ~]$ sudo cat /etc/systemd/system/web.service 
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
User=web
WorkingDirectory=/srv

[Install]
WantedBy=multi-user.target
```



### Curl

Le service web fonctionne bien.

```bash
[flo@tp1 ~]$ curl 10.0.1.170:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="test">test</a>
</ul>
<hr>
</body>
</html>
```



### History

Résultat de history : (un peu simplifié)

```bash
    4  ls /etc/sysconfig/network-scripts/
    5  cd /etc/sysconfig/network-scripts/
    6  sudo vi ifcfg-ens192 
    7  sudo ifup ifcfg-ens192
   19  sudo vi /etc/resolv.conf
   20  cat /etc/resolv.conf 
   22  sudo vi ifcfg-ens224
   23  sudo ifup ens224
   25  ip a
   31  yum update
   32  sudo yum update
   33  exit
   34  ssh-keygen -t rsa -b 4096
   38  cd .ssh
   41  sudo cd authorized_keys 
   42  sudo cat authorized_keys 
   44  ls -al
   45  pwd
   46  sudo yum install vim
   47  sudo setenforce 0
   48  sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
   53  sudo useradd tata
   54  cat /etc/passwd
   55  man useradd
   58  sudo useradd toto -m -s /bin/sh
   59  cat /etc/passwd
   61  cd
   63  vim test
   64  ls
   65  cat /etc/group
   66  getent group
   67  groups flo
   68  groupadd admins
   69  sudo groupadd admins
   75  sudo usermod -aG admins toto
   76  sudo usermod -aG admins tata
   77  sudo usermod -aG admins flo
   79  sudo cat /etc/sudoers
   81  sudo vi /etc/sudoers
   82  sudo cat /etc/sudoers
   83  cat /etc/resolv.conf 
   84  hostname
   85  cat /etc/hostname 
   86  sudo vim /etc/hostname 
   87  hostname
   89  sudo hostname tp1.florian.net
   90  cat /etc/resolv.conf 
   91  man dig
   92  df -h
   93  ls /dev/sd*
   95  groups admin
   97  groups admins
   99  getent group
  100  lsblk
  101  sudo pvcreate /dev/sdb
  102  sudo pvcreate /dev/sdc
  103  sudo pvs
  104  sudo pvdisplay
  105  sudo vgcreate lab /dev/sdb
  106  sudo pvs
  107  sudo pvdisplay
  108  sudo vgextend lab /dev/sdc
  109  sudo pvs
  110  sudo pvdisplay
  111  sudo lvcreate -L 1G lab -n part1
  112  ls /dev/sd*
  113  sudo lvs
  118  man lvremove 
  127  sudo lvdisplay
  138  sudo lvremove /dev/lab/test1
  139  ls /dev/lab/
  140  sudo ls /dev/lab/
  141  sudo ls /dev/
  142  sudo ls /dev/disk/
  143  sudo ls /dev/disk/by-id/
  144  sudo ls -al /dev/disk/by-id/
  145  sudo lvcreate -L 1G lab -n volume1
  146  sudo lvcreate -L 1G lab -n volume2
  147  sudo lvcreate -L 1G lab -n volume3
  148  vgs
  149  sudo vgs
  150  sudo lvs
  151  vgdisplay
  152  sudo vgdisplay
  153  sudo lvdisplay
  155  sudo mkfs -t ext4 /dev/lab/volume1
  156  sudo mkfs -t ext4 /dev/lab/volume2
  157  sudo mkfs -t ext4 /dev/lab/volume3
  158  ls /mnt/
  159  mkdir /mnt/part1
  160  sudo mkdir /mnt/part1
  161  sudo mkdir /mnt/part2
  162  sudo mkdir /mnt/part3
  164  sudo mount /dev/lab/volume1 /mnt/part1
  165  sudo mount /dev/lab/volume2 /mnt/part2
  166  sudo mount /dev/lab/volume3 /mnt/part3
  167  mount
  168  df -h
  169  sudo cat /etc/fstab
  171  sudo lvs
  172  sudo lvdisplay
  173  ls /mnt/
  174  cat /etc/fstab 
  176  sudo vi /etc/fstab 
  177  cat /etc/fstab 
  178  systemctl
  179  systemctl status network
  180  systemctl is-active network
  181  systemctl cat network
  182  systemctl list-units -t service
  183  systemctl status firewalld
  184  sudo firewall-cmd --add-port=8888/tcp --permanent
  185  ls /etc/systemd/system
  186  sudo vi /etc/systemd/system/web.service
  187  ls /etc/systemd/system
  188  sudo systemctl daemon-reload
  189  sudo systemctl status web
  190  sudo systemctl start web
  191  sudo systemctl enable web
  192  sudo systemctl status web
  193  sudo firewall-cmd --reload
  194  useradd web
  195  sudo useradd web
  197  cat /etc/passwd
  198  sudo vi /etc/systemd/system/web.service
  199  ls /srv/
  200  vi /srv/test
  201  sudo vi /srv/test
  202  sudo vi /etc/systemd/system/web.service
  241  history
```

