# Résultat du TP2 de Peyron Florian

## Sommaire

[TOC]



## db.tp2.cesi

### Service mariadb

```bash
[flo@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since mer. 2020-12-16 13:59:13 CET; 3 days ago
 Main PID: 2081 (mysqld_safe)
   CGroup: /system.slice/mariadb.service
           ├─2081 /bin/sh /usr/bin/mysqld_safe --basedir=/usr
           └─2245 /usr/libexec/mysqld --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib64/mysql/plugin --log-error=/var/log/mariadb/mar...

déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: MySQL manual for more instructions.
déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: Please report any problems at http://mariadb.org/jira
déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: The latest information about MariaDB is available at http://mariadb.org/.
déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: You can find additional information about the MySQL part at:
déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: http://dev.mysql.com
déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: Consider joining MariaDB's strong and vibrant community:
déc. 16 13:59:11 db.tp2.cesi mariadb-prepare-db-dir[1997]: https://mariadb.org/get-involved/
déc. 16 13:59:11 db.tp2.cesi mysqld_safe[2081]: 201216 13:59:11 mysqld_safe Logging to '/var/log/mariadb/mariadb.log'.
déc. 16 13:59:11 db.tp2.cesi mysqld_safe[2081]: 201216 13:59:11 mysqld_safe Starting mysqld daemon with databases from /var/lib/mysql
déc. 16 13:59:13 db.tp2.cesi systemd[1]: Started MariaDB database server.
```



### Base de données

```bash
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| Wordpress          |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)
```



### Utilisateurs de base de données

```bash
MariaDB [(none)]> select user, host from mysql.user;
+-----------+-------------+
| user      | host        |
+-----------+-------------+
| flo       | %           |
| wordpress | %           |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | db.tp2.cesi |
| root      | localhost   |
+-----------+-------------+
6 rows in set (0.01 sec)
```



### Liste des ports en écoute

```bash
[flo@db ~]$ sudo ss -alnpt
[sudo] Mot de passe de flo : 
State      Recv-Q Send-Q                              Local Address:Port                                             Peer Address:Port              
LISTEN     0      50                                              *:3306                                                        *:*                   users:(("mysqld",pid=2245,fd=14))
LISTEN     0      128                                             *:22                                                          *:*                   users:(("sshd",pid=1255,fd=3))
LISTEN     0      5                                               *:8888                                                        *:*                   users:(("python2",pid=853,fd=3))
LISTEN     0      100                                     127.0.0.1:25                                                          *:*                   users:(("master",pid=1343,fd=13))
LISTEN     0      128                                          [::]:80                                                       [::]:*                   users:(("httpd",pid=6934,fd=4),("httpd",pid=6930,fd=4),("httpd",pid=6925,fd=4),("httpd",pid=4862,fd=4),("httpd",pid=3105,fd=4),("httpd",pid=3104,fd=4),("httpd",pid=3103,fd=4),("httpd",pid=3076,fd=4),("httpd",pid=3073,fd=4),("httpd",pid=2976,fd=4),("httpd",pid=2970,fd=4))
LISTEN     0      128                                          [::]:22                                                       [::]:*                   users:(("sshd",pid=1255,fd=4))
LISTEN     0      100                                         [::1]:25                                                       [::]:*                   users:(("master",pid=1343,fd=14))
```



### Port du Firewall

```bash
[flo@db ~]$ sudo firewall-cmd --list-all
[sudo] Mot de passe de flo : 
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192 ens224
  sources: 
  services: dhcpv6-client http https ssh
  ports: 8888/tcp 443/tcp 80/tcp 3306/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

Le port 80 est pour phpMyAdmin



### PhpMyAdmin

```html
<!DOCTYPE HTML>
<html lang='fr' dir='ltr' class='safari safari14'>
<head>
    ..........
    </style>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    ..........
    <title>phpMyAdmin</title>
    ..........
    <script data-cfasync="false" type="text/javascript">
    // <![CDATA[
    PMA_commonParams.setAll({
        common_query: "?lang=fr&collation_connection=utf8_unicode_ci&token=da61d3797a435e4a38ed1bae606d584b",
        opendb_url: "db_structure.php",
        ..........
        show_databases_navigation_as_tree: "1",
        pma_absolute_uri: "http://10.0.1.171/phpmyadmin/",
        pma_text_default_tab: "Afficher",
        ..........
        auth_type: "cookie"
    });
    ..........
    $(function() {
        AJAX.fireOnload("whitelist.php?lang=fr&amp .......... ");
        AJAX.fireOnload("sprintf.js");
        ..........
    });
    // ]]>
    </script>
    ..........
</head>
<body id='loginform'>
    <div id="page_content">
        <div class="container">
            <a href="./url.php?url=https%3A%2F%2Fwww.phpmyadmin.net%2F" target="_blank" class="logo">
                <img src="./themes/pmahomme/img/logo_right.png" id="imLogo" name="imLogo" alt="phpMyAdmin" border="0"/>
            </a>
            <h1>
                Bienvenue dans 
                <bdo dir="ltr" lang="en">phpMyAdmin</bdo>
            </h1>
            ..........
            <div class='hide js-show'>
                <form method="get" action="index.php" class="disableAjax">
                    ..........
                    <fieldset>
                        <legend lang="en" dir="ltr">
                            Langue - 
                            <em>Language</em>
                        </legend>
                        <select name="lang" class="autosubmit" lang="en" dir="ltr" id="sel-lang">
                            ..........
                            <option value="fi">Suomi - Finnish</option>
                            <option value="fr" selected="selected">Fran&ccedil;ais - French</option>
                            ..........
                        </select>
                    </fieldset>
                </form>
            </div>
            <br/>
            <!-- Login form -->
            <form method="post" action="index.php" name="login_form" autocomplete="off" class="disableAjax login hide js-show">
                <fieldset>
                    <legend>
                        Connexion
                        <a href="./doc/html/index.html" target="documentation">
                            <img src="themes/dot.gif" title="Documentation" alt="Documentation" class="icon ic_b_help"/>
                        </a>
                    </legend>
                    <div class="item">
                        <label for="input_username">Utilisateur : </label>
                        <input type="text" name="pma_username" id="input_username" value="" size="24" class="textfield"/>
                    </div>
                    <div class="item">
                        <label for="input_password">Mot de passe : </label>
                        <input type="password" name="pma_password" id="input_password" value="" size="24" class="textfield"/>
                    </div>
                    <input type="hidden" name="server" value="1"/>
                </fieldset>
                <fieldset class="tblFooters">
                    ..........
                </fieldset>
            </form>
        </div>
    </div>
</body>
</html>


```

Raccourci avec des ..........



## web.tp2.cesi

### Service HTTPd

````bash
[flo@web ~]$ sudo systemctl status httpd
[sudo] Mot de passe de flo : 
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since mer. 2020-12-16 15:49:57 CET; 2 days ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 2353 (httpd)
   Status: "Total requests: 606; Current requests/sec: 0; Current traffic:   0 B/sec"
   CGroup: /system.slice/httpd.service
           ├─2353 /usr/sbin/httpd -DFOREGROUND
           ├─2356 /usr/sbin/httpd -DFOREGROUND
           ├─2358 /usr/sbin/httpd -DFOREGROUND
           ├─2424 /usr/sbin/httpd -DFOREGROUND
           ├─2425 /usr/sbin/httpd -DFOREGROUND
           ├─2445 /usr/sbin/httpd -DFOREGROUND
           ├─2447 /usr/sbin/httpd -DFOREGROUND
           ├─3554 /usr/sbin/httpd -DFOREGROUND
           ├─3556 /usr/sbin/httpd -DFOREGROUND
           ├─3580 /usr/sbin/httpd -DFOREGROUND
           └─3581 /usr/sbin/httpd -DFOREGROUND

déc. 16 15:49:57 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
déc. 16 15:49:57 web.tp2.cesi httpd[2353]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using we... message
déc. 16 15:49:57 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Hint: Some lines were ellipsized, use -l to show in full.
````



### Fichier de configuration de HTTPd

```bash
[flo@web ~]$ cat /etc/httpd/conf/httpd.conf 
#
# This is the main Apache HTTP server configuration file.  It contains the
# configuration directives that give the server its instructions.
# See <URL:http://httpd.apache.org/docs/2.4/> for detailed information.
# In particular, see 
# <URL:http://httpd.apache.org/docs/2.4/mod/directives.html>
# for a discussion of each configuration directive.
#
# Do NOT simply read the instructions in here without understanding
# what they do.  They're here only as hints or reminders.  If you are unsure
# consult the online docs. You have been warned.  
#
# Configuration and logfile names: If the filenames you specify for many
# of the server's control files begin with "/" (or "drive:/" for Win32), the
# server will use that explicit path.  If the filenames do *not* begin
# with "/", the value of ServerRoot is prepended -- so 'log/access_log'
# with ServerRoot set to '/www' will be interpreted by the
# server as '/www/log/access_log', where as '/log/access_log' will be
# interpreted as '/log/access_log'.

#
# ServerRoot: The top of the directory tree under which the server's
# configuration, error, and log files are kept.
#
# Do not add a slash at the end of the directory path.  If you point
# ServerRoot at a non-local disk, be sure to specify a local disk on the
# Mutex directive, if file-based mutexes are used.  If you wish to share the
# same ServerRoot for multiple httpd daemons, you will need to change at
# least PidFile.
#
ServerRoot "/etc/httpd"

#
# Listen: Allows you to bind Apache to specific IP addresses and/or
# ports, instead of the default. See also the <VirtualHost>
# directive.
#
# Change this to Listen on specific IP addresses as shown below to 
# prevent Apache from glomming onto all bound IP addresses.
#
#Listen 12.34.56.78:80
Listen 80

#
# Dynamic Shared Object (DSO) Support
#
# To be able to use the functionality of a module which was built as a DSO you
# have to place corresponding `LoadModule' lines at this location so the
# directives contained in it are actually available _before_ they are used.
# Statically compiled modules (those listed by `httpd -l') do not need
# to be loaded here.
#
# Example:
# LoadModule foo_module modules/mod_foo.so
#
Include conf.modules.d/*.conf

#
# If you wish httpd to run as a different user or group, you must run
# httpd as root initially and it will switch.  
#
# User/Group: The name (or #number) of the user/group to run httpd as.
# It is usually good practice to create a dedicated user and group for
# running httpd, as with most system services.
#
User apache
Group apache

# 'Main' server configuration
#
# The directives in this section set up the values used by the 'main'
# server, which responds to any requests that aren't handled by a
# <VirtualHost> definition.  These values also provide defaults for
# any <VirtualHost> containers you may define later in the file.
#
# All of these directives may appear inside <VirtualHost> containers,
# in which case these default settings will be overridden for the
# virtual host being defined.
#

#
# ServerAdmin: Your address, where problems with the server should be
# e-mailed.  This address appears on some server-generated pages, such
# as error documents.  e.g. admin@your-domain.com
#
ServerAdmin root@localhost

#
# ServerName gives the name and port that the server uses to identify itself.
# This can often be determined automatically, but we recommend you specify
# it explicitly to prevent problems during startup.
#
# If your host doesn't have a registered DNS name, enter its IP address here.
#
#ServerName www.example.com:80

#
# Deny access to the entirety of your server's filesystem. You must
# explicitly permit access to web content directories in other 
# <Directory> blocks below.
#
<Directory />
    AllowOverride none
    Require all denied
</Directory>

#
# Note that from this point forward you must specifically allow
# particular features to be enabled - so if something's not working as
# you might expect, make sure that you have specifically enabled it
# below.
#

#
# DocumentRoot: The directory out of which you will serve your
# documents. By default, all requests are taken from this directory, but
# symbolic links and aliases may be used to point to other locations.
#
DocumentRoot "/var/www/html/wordpress"

#
# Relax access to content within /var/www.
#
<Directory "/var/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>

# Further relax access to the default document root:
<Directory "/var/www/html/wordpress">
    #
    # Possible values for the Options directive are "None", "All",
    # or any combination of:
    #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
    #
    # Note that "MultiViews" must be named *explicitly* --- "Options All"
    # doesn't give it to you.
    #
    # The Options directive is both complicated and important.  Please see
    # http://httpd.apache.org/docs/2.4/mod/core.html#options
    # for more information.
    #
    Options Indexes FollowSymLinks

    #
    # AllowOverride controls what directives may be placed in .htaccess files.
    # It can be "All", "None", or any combination of the keywords:
    #   Options FileInfo AuthConfig Limit
    #
    AllowOverride None

    #
    # Controls who can get stuff from this server.
    #
    Require all granted
</Directory>

#
# DirectoryIndex: sets the file that Apache will serve if a directory
# is requested.
#
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

#
# The following lines prevent .htaccess and .htpasswd files from being 
# viewed by Web clients. 
#
<Files ".ht*">
    Require all denied
</Files>

#
# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.  If you *do* define an error logfile for a <VirtualHost>
# container, that host's errors will be logged there and not here.
#
ErrorLog "logs/error_log"

#
# LogLevel: Control the number of messages logged to the error_log.
# Possible values include: debug, info, notice, warn, error, crit,
# alert, emerg.
#
LogLevel warn

<IfModule log_config_module>
    #
    # The following directives define some format nicknames for use with
    # a CustomLog directive (see below).
    #
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      # You need to enable mod_logio.c to use %I and %O
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>

    #
    # The location and format of the access logfile (Common Logfile Format).
    # If you do not define any access logfiles within a <VirtualHost>
    # container, they will be logged here.  Contrariwise, if you *do*
    # define per-<VirtualHost> access logfiles, transactions will be
    # logged therein and *not* in this file.
    #
    #CustomLog "logs/access_log" common

    #
    # If you prefer a logfile with access, agent, and referer information
    # (Combined Logfile Format) you can use the following directive.
    #
    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>
    #
    # Redirect: Allows you to tell clients about documents that used to 
    # exist in your server's namespace, but do not anymore. The client 
    # will make a new request for the document at its new location.
    # Example:
    # Redirect permanent /foo http://www.example.com/bar

    #
    # Alias: Maps web paths into filesystem paths and is used to
    # access content that does not live under the DocumentRoot.
    # Example:
    # Alias /webpath /full/filesystem/path
    #
    # If you include a trailing / on /webpath then the server will
    # require it to be present in the URL.  You will also likely
    # need to provide a <Directory> section to allow access to
    # the filesystem path.

    #
    # ScriptAlias: This controls which directories contain server scripts. 
    # ScriptAliases are essentially the same as Aliases, except that
    # documents in the target directory are treated as applications and
    # run by the server when requested rather than as documents sent to the
    # client.  The same rules about trailing "/" apply to ScriptAlias
    # directives as to Alias.
    #
    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

#
# "/var/www/cgi-bin" should be changed to whatever your ScriptAliased
# CGI directory exists, if you have that configured.
#
<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    #
    # TypesConfig points to the file containing the list of mappings from
    # filename extension to MIME-type.
    #
    TypesConfig /etc/mime.types

    #
    # AddType allows you to add to or override the MIME configuration
    # file specified in TypesConfig for specific file types.
    #
    #AddType application/x-gzip .tgz
    #
    # AddEncoding allows you to have certain browsers uncompress
    # information on the fly. Note: Not all browsers support this.
    #
    #AddEncoding x-compress .Z
    #AddEncoding x-gzip .gz .tgz
    #
    # If the AddEncoding directives above are commented-out, then you
    # probably should define those extensions to indicate media types:
    #
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz

    #
    # AddHandler allows you to map certain file extensions to "handlers":
    # actions unrelated to filetype. These can be either built into the server
    # or added with the Action directive (see below)
    #
    # To use CGI scripts outside of ScriptAliased directories:
    # (You will also need to add "ExecCGI" to the "Options" directive.)
    #
    #AddHandler cgi-script .cgi

    # For type maps (negotiated resources):
    #AddHandler type-map var

    #
    # Filters allow you to process content before it is sent to the client.
    #
    # To parse .shtml files for server-side includes (SSI):
    # (You will also need to add "Includes" to the "Options" directive.)
    #
    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

#
# Specify a default charset for all content served; this enables
# interpretation of all content as UTF-8 by default.  To use the 
# default browser choice (ISO-8859-1), or to allow the META tags
# in HTML content to override this choice, comment out this
# directive:
#
AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    #
    # The mod_mime_magic module allows the server to use various hints from the
    # contents of the file itself to determine its type.  The MIMEMagicFile
    # directive tells the module where the hint definitions are located.
    #
    MIMEMagicFile conf/magic
</IfModule>

#
# Customizable error responses come in three flavors:
# 1) plain text 2) local redirects 3) external redirects
#
# Some examples:
#ErrorDocument 500 "The server made a boo boo."
#ErrorDocument 404 /missing.html
#ErrorDocument 404 "/cgi-bin/missing_handler.pl"
#ErrorDocument 402 http://www.example.com/subscription_info.html
#

#
# EnableMMAP and EnableSendfile: On systems that support it, 
# memory-mapping or the sendfile syscall may be used to deliver
# files.  This usually improves server performance, but must
# be turned off when serving from networked-mounted 
# filesystems or if support for these functions is otherwise
# broken on your system.
# Defaults if commented: EnableMMAP On, EnableSendfile Off
#
#EnableMMAP off
EnableSendfile on

# Supplemental configuration
#
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```





### Dossier contenant Wordpress

```bash
[flo@web ~]$ ls /var/www/html/wordpress
index.php    wp-activate.php     wp-comments-post.php  wp-content   wp-links-opml.php  wp-mail.php      wp-trackback.php
license.txt  wp-admin            wp-config.php         wp-cron.php  wp-load.php        wp-settings.php  xmlrpc.php
readme.html  wp-blog-header.php  wp-config-sample.php  wp-includes  wp-login.php       wp-signup.php
```



### Fichier de configuration WordPress

Ce fichier est le fichier de configuration pour l'installation de WordPress.

```bash
[flo@web ~]$ cat /var/www/html/wordpress/wp-config.php 
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', '....haha....' );

/** MySQL hostname */
define( 'DB_HOST', '10.0.1.171' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
```



### Port du Firewall

```bash
[flo@web ~]$ sudo firewall-cmd --list-all
[sudo] Mot de passe de flo : 
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192 ens224
  sources: 
  services: dhcpv6-client ssh
  ports: 8888/tcp 3306/tcp 443/tcp 80/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```



### La page par défaut de WordPress

La page "Hello word" s'affiche correctement

```bash
[flo@web ~]$ curl http://10.0.1.170/
<!doctype html>
<html lang="en-US" >
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>TP-CESI &#8211; Just another WordPress site</title>
<meta name='robots' content='noindex,nofollow' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="TP-CESI &raquo; Feed" href="http://10.0.1.170/index.php/feed/" />
<link rel="alternate" type="application/rss+xml" title="TP-CESI &raquo; Comments Feed" href="http://10.0.1.170/index.php/comments/feed/" />
		<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/10.0.1.170\/wordpress\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='http://10.0.1.170/wp-includes/css/dist/block-library/style.min.css?ver=5.6' media='all' />
<link rel='stylesheet' id='wp-block-library-theme-css'  href='http://10.0.1.170/wp-includes/css/dist/block-library/theme.min.css?ver=5.6' media='all' />
<link rel='stylesheet' id='twenty-twenty-one-style-css'  href='http://10.0.1.170/wp-content/themes/twentytwentyone/style.css?ver=1.0' media='all' />
<link rel='stylesheet' id='twenty-twenty-one-print-style-css'  href='http://10.0.1.170/wp-content/themes/twentytwentyone/assets/css/print.css?ver=1.0' media='print' />
<link rel="https://api.w.org/" href="http://10.0.1.170/index.php/wp-json/" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://10.0.1.170/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://10.0.1.170/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.6" />
<style>.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style></head>

<body class="home blog wp-embed-responsive is-light-theme no-js hfeed">
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

	
<header id="masthead" class="site-header has-title-and-tagline" role="banner">

	

<div class="site-branding">

	
						<h1 class="site-title">TP-CESI</h1>
			
			<p class="site-description">
			Just another WordPress site		</p>
	</div><!-- .site-branding -->
	

</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

<article id="post-1" class="post-1 post type-post status-publish format-standard hentry category-uncategorized entry">

	
<header class="entry-header">
	<h2 class="entry-title default-max-width"><a href="http://10.0.1.170/index.php/2020/12/16/hello-world/">Hello world!</a></h2></header><!-- .entry-header -->

	<div class="entry-content">
		<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>
	</div><!-- .entry-content -->

	<footer class="entry-footer default-max-width">
		<span class="posted-on">Published <time class="entry-date published updated" datetime="2020-12-16T15:57:00+00:00">December 16, 2020</time></span><div class="post-taxonomies"><span class="cat-links">Categorized as <a href="http://10.0.1.170/index.php/category/uncategorized/" rel="category tag">Uncategorized</a> </span></div>	</footer><!-- .entry-footer -->
</article><!-- #post-${ID} -->
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

	
	<aside class="widget-area">
		<section id="search-2" class="widget widget_search"><form role="search"  method="get" class="search-form" action="http://10.0.1.170/">
	<label for="search-form-1">Search&hellip;</label>
	<input type="search" id="search-form-1" class="search-field" value="" name="s" />
	<input type="submit" class="search-submit" value="Search" />
</form>
</section>
		<section id="recent-posts-2" class="widget widget_recent_entries">
		<h2 class="widget-title">Recent Posts</h2><nav role="navigation" aria-label="Recent Posts">
		<ul>
											<li>
					<a href="http://10.0.1.170/index.php/2020/12/16/hello-world/">Hello world!</a>
									</li>
					</ul>

		</nav></section><section id="recent-comments-2" class="widget widget_recent_comments"><h2 class="widget-title">Recent Comments</h2><nav role="navigation" aria-label="Recent Comments"><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a href='https://wordpress.org/' rel='external nofollow ugc' class='url'>A WordPress Commenter</a></span> on <a href="http://10.0.1.170/index.php/2020/12/16/hello-world/#comment-1">Hello world!</a></li></ul></nav></section>	</aside><!-- .widget-area -->


	<footer id="colophon" class="site-footer" role="contentinfo">

				<div class="site-info">
			<div class="site-name">
																			TP-CESI																		</div><!-- .site-name -->
			<div class="powered-by">
				Proudly powered by <a href="https://wordpress.org/">WordPress</a>.			</div><!-- .powered-by -->

		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<script>document.body.classList.remove("no-js");</script>	<script>
	if ( -1 !== navigator.userAgent.indexOf( 'MSIE' ) || -1 !== navigator.appVersion.indexOf( 'Trident/' ) ) {
		document.body.classList.add( 'is-IE' );
	}
	</script>
	<script src='http://10.0.1.170/wp-content/themes/twentytwentyone/assets/js/polyfills.js?ver=1.0' id='twenty-twenty-one-ie11-polyfills-js'></script>
<script src='http://10.0.1.170/wp-content/themes/twentytwentyone/assets/js/responsive-embeds.js?ver=1.0' id='twenty-twenty-one-responsive-embeds-script-js'></script>
<script src='http://10.0.1.170/wp-includes/js/wp-embed.min.js?ver=5.6' id='wp-embed-js'></script>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
	</script>
	
</body>
</html>
```



## rp.tp2.cesi

### Service nginx

```bash
[flo@rp ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2020-12-18 10:40:12 CET; 1 day 4h ago
  Process: 1273 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1256 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1251 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1275 (nginx)
   CGroup: /system.slice/nginx.service
           ├─1275 nginx: master process /usr/sbin/nginx
           └─1276 nginx: worker process

déc. 18 10:40:10 rp.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
déc. 18 10:40:12 rp.tp2.cesi nginx[1256]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
déc. 18 10:40:12 rp.tp2.cesi nginx[1256]: nginx: configuration file /etc/nginx/nginx.conf test is successful
déc. 18 10:40:12 rp.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
```



### Conf de nginx

Fichier de configuration de nginx

```bash
[flo@rp ~]$ cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

#user nginx;
#worker_processes auto;
#error_log /var/log/nginx/error.log;
#pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
#include /usr/share/nginx/modules/*.conf;

#events {
#    worker_connections 1024;
#}
#
#http {
#    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
#                      '$status $body_bytes_sent "$http_referer" '
#                      '"$http_user_agent" "$http_x_forwarded_for"';
#
#    access_log  /var/log/nginx/access.log  main;
#
#    sendfile            on;
#    tcp_nopush          on;
#    tcp_nodelay         on;
#    keepalive_timeout   65;
#    types_hash_max_size 2048;
#
#    include             /etc/nginx/mime.types;
#    default_type        application/octet-stream;
#
#    # Load modular configuration files from the /etc/nginx/conf.d directory.
#    # See http://nginx.org/en/docs/ngx_core_module.html#include
#    # for more information.
#    include /etc/nginx/conf.d/*.conf;
#
#    server {
#        listen       80 default_server;
#        listen       [::]:80 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#        location = /404.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#        location = /50x.html {
#        }
#    }
#
# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers HIGH:!aNULL:!MD5;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#        location = /404.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#        location = /50x.html {
#        }
#    }
#
#}

events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        location / {
            proxy_pass   http://10.0.1.170:80;
        }
    }
}

```



### Port du Firewall

```bash
[flo@rp ~]$ sudo firewall-cmd --list-all
[sudo] Mot de passe de flo : 
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192 ens224
  sources: 
  services: dhcpv6-client ssh
  ports: 8888/tcp 80/tcp 443/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```



### Accès à au site au travers du proxy 

La page "Hello word" s'affiche correctement

```bash
[flo@rp ~]$ curl http://10.0.1.172/
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="http://10.0.1.170/">here</a>.</p>
</body></html>
[flo@rp ~]$ curl http://10.0.1.172/wordpress/
<!doctype html>
<html lang="en-US" >
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>TP-CESI &#8211; Just another WordPress site</title>
<meta name='robots' content='noindex,nofollow' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="TP-CESI &raquo; Feed" href="http://10.0.1.170/index.php/feed/" />
<link rel="alternate" type="application/rss+xml" title="TP-CESI &raquo; Comments Feed" href="http://10.0.1.170/index.php/comments/feed/" />
		<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/10.0.1.170\/wordpress\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='http://10.0.1.170/wp-includes/css/dist/block-library/style.min.css?ver=5.6' media='all' />
<link rel='stylesheet' id='wp-block-library-theme-css'  href='http://10.0.1.170/wp-includes/css/dist/block-library/theme.min.css?ver=5.6' media='all' />
<link rel='stylesheet' id='twenty-twenty-one-style-css'  href='http://10.0.1.170/wp-content/themes/twentytwentyone/style.css?ver=1.0' media='all' />
<link rel='stylesheet' id='twenty-twenty-one-print-style-css'  href='http://10.0.1.170/wp-content/themes/twentytwentyone/assets/css/print.css?ver=1.0' media='print' />
<link rel="https://api.w.org/" href="http://10.0.1.170/index.php/wp-json/" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://10.0.1.170/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://10.0.1.170/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.6" />
<style>.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style></head>

<body class="home blog wp-embed-responsive is-light-theme no-js hfeed">
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

	
<header id="masthead" class="site-header has-title-and-tagline" role="banner">

	

<div class="site-branding">

	
						<h1 class="site-title">TP-CESI</h1>
			
			<p class="site-description">
			Just another WordPress site		</p>
	</div><!-- .site-branding -->
	

</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

<article id="post-1" class="post-1 post type-post status-publish format-standard hentry category-uncategorized entry">

	
<header class="entry-header">
	<h2 class="entry-title default-max-width"><a href="http://10.0.1.170/index.php/2020/12/16/hello-world/">Hello world!</a></h2></header><!-- .entry-header -->

	<div class="entry-content">
		<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>
	</div><!-- .entry-content -->

	<footer class="entry-footer default-max-width">
		<span class="posted-on">Published <time class="entry-date published updated" datetime="2020-12-16T15:57:00+00:00">December 16, 2020</time></span><div class="post-taxonomies"><span class="cat-links">Categorized as <a href="http://10.0.1.170/index.php/category/uncategorized/" rel="category tag">Uncategorized</a> </span></div>	</footer><!-- .entry-footer -->
</article><!-- #post-${ID} -->
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

	
	<aside class="widget-area">
		<section id="search-2" class="widget widget_search"><form role="search"  method="get" class="search-form" action="http://10.0.1.170/">
	<label for="search-form-1">Search&hellip;</label>
	<input type="search" id="search-form-1" class="search-field" value="" name="s" />
	<input type="submit" class="search-submit" value="Search" />
</form>
</section>
		<section id="recent-posts-2" class="widget widget_recent_entries">
		<h2 class="widget-title">Recent Posts</h2><nav role="navigation" aria-label="Recent Posts">
		<ul>
											<li>
					<a href="http://10.0.1.170/index.php/2020/12/16/hello-world/">Hello world!</a>
									</li>
					</ul>

		</nav></section><section id="recent-comments-2" class="widget widget_recent_comments"><h2 class="widget-title">Recent Comments</h2><nav role="navigation" aria-label="Recent Comments"><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a href='https://wordpress.org/' rel='external nofollow ugc' class='url'>A WordPress Commenter</a></span> on <a href="http://10.0.1.170/index.php/2020/12/16/hello-world/#comment-1">Hello world!</a></li></ul></nav></section>	</aside><!-- .widget-area -->


	<footer id="colophon" class="site-footer" role="contentinfo">

				<div class="site-info">
			<div class="site-name">
																			TP-CESI																		</div><!-- .site-name -->
			<div class="powered-by">
				Proudly powered by <a href="https://wordpress.org/">WordPress</a>.			</div><!-- .powered-by -->

		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<script>document.body.classList.remove("no-js");</script>	<script>
	if ( -1 !== navigator.userAgent.indexOf( 'MSIE' ) || -1 !== navigator.appVersion.indexOf( 'Trident/' ) ) {
		document.body.classList.add( 'is-IE' );
	}
	</script>
	<script src='http://10.0.1.170/wp-content/themes/twentytwentyone/assets/js/polyfills.js?ver=1.0' id='twenty-twenty-one-ie11-polyfills-js'></script>
<script src='http://10.0.1.170/wp-content/themes/twentytwentyone/assets/js/responsive-embeds.js?ver=1.0' id='twenty-twenty-one-responsive-embeds-script-js'></script>
<script src='http://10.0.1.170/wp-includes/js/wp-embed.min.js?ver=5.6' id='wp-embed-js'></script>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
	</script>
	
</body>
</html>
```



## Fail2ban

Fail2ban a été installé et configuré sur les 3 serveurs que sont : web.tp2.cesi - db.tp2.cesi - rp.tp2.cesi

CF. chapitre History 

### Service fail2ban

```bash
[flo@rp ~]$ systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2020-12-18 10:40:10 CET; 1 day 5h ago
     Docs: man:fail2ban(1)
  Process: 1250 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 1258 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─1258 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start

déc. 18 10:40:10 rp.tp2.cesi systemd[1]: Starting Fail2Ban Service...
déc. 18 10:40:10 rp.tp2.cesi systemd[1]: Started Fail2Ban Service.
déc. 18 10:40:12 rp.tp2.cesi fail2ban-server[1258]: Server ready
```



### Fichier de configuration fail2ban

````bash
[flo@rp ~]$ cat /etc/fail2ban/jail.local 
[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled	    = true
port        = ssh
filter      = sshd
logpath     = /var/log/auth.log
maxretry    = 3
bantime     = 30
````



### Test de connexion

```bash
[flo@db ~]$ ssh flo@10.0.1.172
flo@10.0.1.172's password: 
Permission denied, please try again.
flo@10.0.1.172's password: 
Permission denied, please try again.
flo@10.0.1.172's password: 
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[flo@db ~]$ ssh flo@10.0.1.172
ssh: connect to host 10.0.1.172 port 22: Connection refused
[flo@db ~]$ ssh flo@10.0.1.172
flo@10.0.1.172's password: 
Last failed login: Sat Dec 19 16:09:02 CET 2020 from db on ssh:notty
There were 3 failed login attempts since the last successful login.
Last login: Fri Dec 18 10:52:02 2020 from 10.0.1.71
[flo@rp ~]$ 
```

30 secondes après le "Connection refused", cela fonctionne.



## History

### web.tp2.cesi

```bash
  289  sudo yum install httpd
  296  cd /var/www/html/
  297  ls
  304  sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
  305  sudo yum install -y yum-utils
  306  sudo yum remove -y php
  307  sudo yum-config-manager --enable remi-php56
  308  sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
  310  sudo tar -zxf wordpress-5.6.tar 
  311  ls
  312  sudo tar -xf wordpress-5.6.tar 
  313  ls
  314  sudo systemctl enable httpd
  315  sudo systemctl start httpd
  316  sudo firewall-cmd --add-port=3306/tcp --permanent
  317  sudo firewall-cmd --add-port=443/tcp --permanent
  318  sudo firewall-cmd --add-port=80/tcp --permanent
  319  sudo firewall-cmd --reload
  322  cd wordpress
  323  cat readme.html 
  326  sudo cp wp-config-sample.php wp-config.php
  328  sudo vi wp-config.php
  341  sudo yum install epel-release
  342  sudo yum install fail2ban
  343  sudo systemctl enable fail2ban
  345  ls /etc/fail2ban/
  346  sudo vi /etc/fail2ban/jail.local
  348  cat /etc/fail2ban/jail.local
  349  sudo systemctl start fail2ban
  350  sudo systemctl status fail2ban

```



### db.tp2.cesi

```bash
  305  sudo yum install mariadb-server
  308  sudo systemctl start mariadb.service
  310  systemctl enable mariadb.service
  311  systemctl status mariadb.service
  314  mysql_secure_installation
  317  mysql -u root -p
  319  sudo yum install epel-release
  320  sudo yum install phpmyadmin -y
  321  sudo vi /etc/httpd/conf.d/phpMyAdmin.conf
  338  sudo yum install httpd
  339  sudo vi /etc/httpd/conf.d/phpMyAdmin.conf
  340  sudo yum install php
  341  sudo systemctl restart httpd
  345  sudo firewall-cmd --list-all
  346  sudo firewall-cmd --add-port=443/tcp --permanent
  347  sudo firewall-cmd --add-port=80/tcp --permanent
  348  sudo firewall-cmd --reload
  349  sudo firewall-cmd --list-all
  350  sudo ss -alnpt
  351  sudo firewall-cmd --add-port=3306/tcp --permanent
  352  sudo firewall-cmd --reload
  354  sudo firewall-cmd --list-all
  360  sudo yum install fail2ban
  361  sudo systemctl enable fail2ban
  362  systemctl status fail2ban
  363  sudo vi /etc/fail2ban/jail.local
  364  cat /etc/fail2ban/jail.local
  365  sudo systemctl start fail2ban
  366  sudo systemctl status fail2ban
  367  sudo yum install bind-utils
```



### rp.tp2.cesi

```bash
  298  sudo yum install epel-release
  299  sudo yum install nginx
  300  cat /etc/nginx/nginx.conf
  301  sudo vi /etc/nginx/nginx.conf
  304  sudo systemctl enable nginx
  309  systemctl start nginx
  312  ls /usr/share/nginx/modules
  320  sudo firewall-cmd --reload
  321  sudo firewall-cmd --list-all
  322  sudo vi /etc/nginx/nginx.conf
  328  systemctl stop nginx
  329  systemctl start nginx
  330  sudo yum install fail2ban
  331  sudo systemctl enable fail2ban
  332  ls /etc/fail2ban/
  335  cat /etc/fail2ban/jail.conf 
  336  sudo vi /etc/fail2ban/jail.local
  338  sudo cat /var/log/secure 
  342  systemctl restart fail2ban
  343  systemctl status fail2ban
```

