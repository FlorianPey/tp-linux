# Résultat du TP3 de Peyron Florian

## Sommaire

[TOC]



## DNS

Configuration réseau de mon lab :

|   Machine    |     IP     |            Rôle             |
| :----------: | :--------: | :-------------------------: |
| web.tp2.cesi | 10.0.1.170 |         Serveur Web         |
| db.tp2.cesi  | 10.0.1.171 | Serveur de bases de données |
| rp.tp2.cesi  | 10.0.1.172 |        Reverse proxy        |
| ns1.tp2.cesi | 10.0.1.173 |         Serveur DNS         |



### Service bind

```bash
[flo@ns1 ~]$ sudo systemctl status named
[sudo] Mot de passe de flo : 
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2020-12-18 15:34:45 CET; 1 day 1h ago
  Process: 2535 ExecStop=/bin/sh -c /usr/sbin/rndc stop > /dev/null 2>&1 || /bin/kill -TERM $MAINPID (code=exited, status=0/SUCCESS)
  Process: 2549 ExecStart=/usr/sbin/named -u named -c ${NAMEDCONF} $OPTIONS (code=exited, status=0/SUCCESS)
  Process: 2546 ExecStartPre=/bin/bash -c if [ ! "$DISABLE_ZONE_CHECKING" == "yes" ]; then /usr/sbin/named-checkconf -z "$NAMEDCONF"; else echo "Checking of zone files is disabled"; fi (code=exited, status=0/SUCCESS)
 Main PID: 2551 (named)
   CGroup: /system.slice/named.service
           └─2551 /usr/sbin/named -u named -c /etc/named.conf

déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:dc3::35#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:12::d0d#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:503:ba3e::2:30#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:200::b#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:2f::f#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:2::c#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:9f::42#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:a8::e#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:7fe::53#53
déc. 19 15:34:46 ns1.tp2.cesi named[2551]: network unreachable resolving '_ta-4f66/NULL/IN': 2001:500:1::53#53
```



### Fichier de configuration de bind

```bash
[flo@ns1 ~]$ sudo cat /etc/named.conf 
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
// See the BIND Administrator's Reference Manual (ARM) for details about the
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

options {
	listen-on port 53 { 127.0.0.1; 10.0.1.173; };
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	recursing-file  "/var/named/data/named.recursing";
	secroots-file   "/var/named/data/named.secroots";
	allow-query     { 10.0.1.0/24; };

	/* 
	 - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
	 - If you are building a RECURSIVE (caching) DNS server, you need to enable 
	   recursion. 
	 - If your recursive DNS server has a public IP address, you MUST enable access 
	   control to limit queries to your legitimate users. Failing to do so will
	   cause your server to become part of large scale DNS amplification 
	   attacks. Implementing BCP38 within your network would greatly
	   reduce such attack surface 
	*/
	recursion yes;

	dnssec-enable yes;
	dnssec-validation yes;

	/* Path to ISC DLV key */
	bindkeys-file "/etc/named.root.key";

	managed-keys-directory "/var/named/dynamic";

	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
	type hint;
	file "named.ca";
};


zone "tp2.cesi" IN {
         
         type master;
        
         file "/var/named/tp2.cesi.db";

         allow-update { none; };
};

zone "1.0.10.in-addr.arpa" IN {
          
          type master;
          
          file "/var/named/1.0.10.db";
         
          allow-update { none; };
};


include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```



### Configuration de la zone tp2.cesi

```bash
[flo@ns1 ~]$ sudo cat /var/named/tp2.cesi.db
$TTL    604800
@   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      ns1.tp2.cesi.

ns1    IN  A       10.0.1.173
web    IN  A       10.0.1.170
db     IN  A       10.0.1.171
rp     IN  A       10.0.1.172
```



### Configuration de la zone inverse

```bash
[flo@ns1 ~]$ sudo cat /var/named/1.0.10.db
$TTL    604800
@   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp2.cesi.

173.1.0.10 IN PTR ns1.tp2.cesi.

;PTR Record IP address to HostName
170      IN  PTR     web.tp2.cesi.
171      IN  PTR     db.tp2.cesi.
172      IN  PTR     rp.tp2.cesi.
```



### Port du Firewall

```bash
[flo@ns1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192 ens224
  sources: 
  services: dhcpv6-client ssh
  ports: 53/tcp 53/udp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```



### Test de résolution de nom

Test effectué depuis le serveur web qui a pour serveur de nom le serveur DNS.

```bash
[flo@web ~]$ dig db.tp2.cesi

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> db.tp2.cesi
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48094
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;db.tp2.cesi.			IN	A

;; ANSWER SECTION:
db.tp2.cesi.		604800	IN	A	10.0.1.171

;; AUTHORITY SECTION:
tp2.cesi.		604800	IN	NS	ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.		604800	IN	A	10.0.1.173

;; Query time: 0 msec
;; SERVER: 10.0.1.173#53(10.0.1.173)
;; WHEN: sam. déc. 19 17:14:53 CET 2020
;; MSG SIZE  rcvd: 90

[flo@web ~]$ 
[flo@web ~]$ dig -x 10.0.1.171

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> -x 10.0.1.171
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 10095
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;171.1.0.10.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
171.1.0.10.in-addr.arpa. 604800	IN	PTR	db.tp2.cesi.

;; AUTHORITY SECTION:
1.0.10.in-addr.arpa.	604800	IN	NS	ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.		604800	IN	A	10.0.1.173

;; Query time: 0 msec
;; SERVER: 10.0.1.173#53(10.0.1.173)
;; WHEN: sam. déc. 19 17:15:11 CET 2020
;; MSG SIZE  rcvd: 111
```





## History

### ns1.tp2.cesi

```bash
  356  sudo yum install dig
  359  sudo yum install bind bind-utils
  360  cat /etc/named.conf 
  361  sudo cat /etc/named.conf 
  362  sudo firewall-cmd --add-port=53/tcp --permanent
  363  sudo firewall-cmd --add-port=53/udp --permanent
  364  sudo firewall-cmd --reload
  370  sudo vi /etc/named.conf 
  371  sudo cat /etc/named.conf 
  372  sudo ls /var/named/
  373  sudo vi /var/named/tp2.cesi.db
  374  sudo vi /var/named/1.0.10.db
  377  sudo systemctl enable named
  378  sudo systemctl start named
  379  sudo systemctl status named
  396  dig web.tp2.cesi @10.0.1.173
  404  sudo firewall-cmd --remove-port=80/tcp --permanent
  405  sudo firewall-cmd --remove-port=443/tcp --permanent
  406  sudo firewall-cmd --remove-port=8888/tcp --permanent
  407  sudo firewall-cmd --reload
  408  sudo firewall-cmd --list-all
```

